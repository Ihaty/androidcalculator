package com.example.calculatorandroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EditText myInputText = (EditText) findViewById(R.id.editTextNumber);
        disableEditText(myInputText);
        myInputText.setText("0");
    }

    public void butClick0(View view) {
        EditText myInputText = (EditText) findViewById(R.id.editTextNumber);
        String output = myInputText.getText().toString();
        if (output.equals("0")) {
            return;
        }
        myInputText.setText(myInputText.getText() + "0");
    }

    public void butClick1(View view) {
        EditText myInputText = (EditText) findViewById(R.id.editTextNumber);
        String output = myInputText.getText().toString();
        if (output.equals("0")) {
            myInputText.setText("");
        }
        myInputText.setText(myInputText.getText() + "1");
    }

    private void disableEditText(EditText myInputText) {
        myInputText.setFocusable(false);
//        myInputText.setEnabled(false);
        myInputText.setCursorVisible(false);
        myInputText.setKeyListener(null);
    }

    public void butClick2(View view) {
        EditText myInputText = (EditText) findViewById(R.id.editTextNumber);
        String output = myInputText.getText().toString();
        if (output.equals("0")) {
            myInputText.setText("");
        }
        myInputText.setText(myInputText.getText() + "2");
    }

    public void butClick3(View view) {
        EditText myInputText = (EditText) findViewById(R.id.editTextNumber);
        String output = myInputText.getText().toString();
        if (output.equals("0")) {
            myInputText.setText("");
        }
        myInputText.setText(myInputText.getText() + "3");
    }

    public void butClick4(View view) {
        EditText myInputText = (EditText) findViewById(R.id.editTextNumber);
        String output = myInputText.getText().toString();
        if (output.equals("0")) {
            myInputText.setText("");
        }
        myInputText.setText(myInputText.getText() + "4");
    }

    public void butClick5(View view) {
        EditText myInputText = (EditText) findViewById(R.id.editTextNumber);
        String output = myInputText.getText().toString();
        if (output.equals("0")) {
            myInputText.setText("");
        }
        myInputText.setText(myInputText.getText() + "5");
    }

    public void butClick6(View view) {
        EditText myInputText = (EditText) findViewById(R.id.editTextNumber);
        String output = myInputText.getText().toString();
        if (output.equals("0")) {
            myInputText.setText("");
        }
        myInputText.setText(myInputText.getText() + "6");
    }

    public void butClick7(View view) {
        EditText myInputText = (EditText) findViewById(R.id.editTextNumber);
        String output = myInputText.getText().toString();
        if (output.equals("0")) {
            myInputText.setText("");
        }
        myInputText.setText(myInputText.getText() + "7");
    }

    public void butClick8(View view) {
        EditText myInputText = (EditText) findViewById(R.id.editTextNumber);
        String output = myInputText.getText().toString();
        if (output.equals("0")) {
            myInputText.setText("");
        }
        myInputText.setText(myInputText.getText() + "8");
    }

    public void butClick9(View view) {
        EditText myInputText = (EditText) findViewById(R.id.editTextNumber);
        String output = myInputText.getText().toString();
        if (output.equals("0")) {
            myInputText.setText("");
        }
        myInputText.setText(myInputText.getText() + "9");
    }

    public void butClick1Plus(View view) {
        EditText myInputText = (EditText) findViewById(R.id.editTextNumber);
        String output = myInputText.getText().toString();
        if (output.equals("0") || output.equals("")) {
            return;
        } else {
            int indexJava = myInputText.getText().toString().indexOf("/");
            int indexJava1 = myInputText.getText().toString().indexOf("-");
            int indexJava2 = myInputText.getText().toString().indexOf("*");
            int indexJava3 = myInputText.getText().toString().indexOf("+");
            int indexJava4 = myInputText.getText().toString().indexOf(".");
            if (indexJava == -1 && indexJava3 == -1 && indexJava1 == -1 && indexJava2 == -1 && indexJava4 == -1) {
                myInputText.setText(myInputText.getText() + "+");
            }
        }
    }

    public void butClickMinus(View view) {
        EditText myInputText = (EditText) findViewById(R.id.editTextNumber);
        String output = myInputText.getText().toString();
        int indexJava = myInputText.getText().toString().indexOf("/");
        int indexJava1 = myInputText.getText().toString().indexOf("-");
        int indexJava2 = myInputText.getText().toString().indexOf("*");
        int indexJava3 = myInputText.getText().toString().indexOf("+");
        int indexJava4 = myInputText.getText().toString().indexOf(".");
        if (indexJava == -1 && indexJava3 == -1 && indexJava1 == -1 && indexJava2 == -1 && indexJava4 == -1) {
            myInputText.setText(myInputText.getText() + "-");
        }
    }

    public void butClickMultiply(View view) {
        EditText myInputText = (EditText) findViewById(R.id.editTextNumber);
        String output = myInputText.getText().toString();
        if (output.equals("0")) {
            return;
        } else {
            int indexJava = myInputText.getText().toString().indexOf("/");
            int indexJava1 = myInputText.getText().toString().indexOf("-");
            int indexJava2 = myInputText.getText().toString().indexOf("*");
            int indexJava3 = myInputText.getText().toString().indexOf("+");
            int indexJava4 = myInputText.getText().toString().indexOf(".");
            if (indexJava == -1 && indexJava3 == -1 && indexJava1 == -1 && indexJava2 == -1 && indexJava4 == -1) {
                myInputText.setText(myInputText.getText() + "*");
            }
        }
    }

    public void butClickSplit(View view) {
        EditText myInputText = (EditText) findViewById(R.id.editTextNumber);
        String output = myInputText.getText().toString();
        if (output.equals("0")) {
            return;
        } else {
            int indexJava = myInputText.getText().toString().indexOf("/");
            int indexJava1 = myInputText.getText().toString().indexOf("-");
            int indexJava2 = myInputText.getText().toString().indexOf("*");
            int indexJava3 = myInputText.getText().toString().indexOf("+");
            int indexJava4 = myInputText.getText().toString().indexOf(".");
            if (indexJava == -1 && indexJava3 == -1 && indexJava1 == -1 && indexJava2 == -1 && indexJava4 == -1) {
                myInputText.setText(myInputText.getText() + "/");
            }
        }
    }

    public void butClickClearAll(View view) {
        EditText myInputText = (EditText) findViewById(R.id.editTextNumber);
        myInputText.setText("0");
    }

    public void butClickClear(View view) {
        EditText myInputText = (EditText) findViewById(R.id.editTextNumber);
        if (myInputText.getText().equals("0")) {
            return;
        } else {
            String colums;
            colums = myInputText.getText().toString();
            colums = colums.substring(0, colums.length() - 1);
            if (myInputText.getText().length() != 1) {
                myInputText.setText(colums);
            } else {
                myInputText.setText("0");
            }
        }
    }

    public void butClickPoint(View view) {
        EditText myInputText = (EditText) findViewById(R.id.editTextNumber);
        String output = myInputText.getText().toString();
        if (output.equals("0")) {
            myInputText.setText("0.");
        } else {
            int indexJava = myInputText.getText().toString().indexOf("/");
            int indexJava1 = myInputText.getText().toString().indexOf("-");
            int indexJava2 = myInputText.getText().toString().indexOf("*");
            int indexJava3 = myInputText.getText().toString().indexOf("+");
            int indexJava4 = myInputText.getText().toString().indexOf(".");
            if (indexJava == -1 && indexJava3 == -1 && indexJava1 == -1 && indexJava2 == -1 && indexJava4 == -1) {
                myInputText.setText(myInputText.getText() + ".");
            }
        }
    }

    public void butClickEqual(View view) {
        EditText myInputText = (EditText) findViewById(R.id.editTextNumber);
        String output = myInputText.getText().toString();
        if (output.equals("0")) {
            return;
        } else {
            boolean on = true;
            String num1 = "";
            int n1 = 0;

            int indexMultiply = output.indexOf("*");
            if (indexMultiply != -1) {
                for (String retval : output.split("\\*")) {
                    String[] in = new String[2];
                    if (on) {
                        in[0] = retval;
                        on = false;
                        num1 = in[0];
                        n1 = Integer.parseInt(num1);
                        continue;
                    }
                    in[1] = retval;
                    String num2 = in[1];
                    int n2 = Integer.parseInt(num2);
                    int resulting = n1 * n2;
                    String resultConvert = Integer.toString(resulting);
                    myInputText.setText(resultConvert);
                }
            }

            int indexPlus = output.indexOf("+");
            if (indexPlus != -1) {
                for (String retval : output.split("\\+")) {
                    String[] in = new String[2];
                    if (on) {
                        in[0] = retval;
                        on = false;
                        num1 = in[0];
                        n1 = Integer.parseInt(num1);
                        continue;
                    }
                    in[1] = retval;
                    String num2 = in[1];
                    int n2 = Integer.parseInt(num2);
                    int resulting = n1 + n2;
                    String resultConvert = Integer.toString(resulting);
                    myInputText.setText(resultConvert);
                }
            }

            int indexMinus = output.indexOf("-");
            if (indexMinus != -1) {
                for (String retval : output.split("-")) {
                    String[] in = new String[2];
                    if (on) {
                        in[0] = retval;
                        on = false;
                        num1 = in[0];
                        n1 = Integer.parseInt(num1);
                        continue;
                    }
                    in[1] = retval;
                    String num2 = in[1];
                    int n2 = Integer.parseInt(num2);
                    int resulting = n1 - n2;
                    String resultConvert = Integer.toString(resulting);
                    myInputText.setText(resultConvert);
                }
            }

            int indexSplice = output.indexOf("/");
            if (indexSplice!= -1) {
                for (String retval : output.split("/")) {
                    String[] in = new String[2];
                    if (on) {
                        in[0] = retval;
                        on = false;
                        num1 = in[0];
                        n1 = Integer.parseInt(num1);
                        continue;
                    }
                    in[1] = retval;
                    String num2 = in[1];
                    int n2 = Integer.parseInt(num2);
                    int resulting = n1 / n2;
                    String resultConvert = Integer.toString(resulting);
                    myInputText.setText(resultConvert);
                }
            }
        }
    }



}